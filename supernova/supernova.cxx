/*=========================================================================

  Program:   Visualization Toolkit
  Module:    SpecularSpheres.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

// Credits: Lindsay Luallen, proj5 (I reused enough of my logic from proj5 that I fel the need to credit myself) 
//
// This examples demonstrates the effect of specular lighting.
//
#include "vtkSmartPointer.h"
#include "vtkSphereSource.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkInteractorStyle.h"
#include "vtkObjectFactory.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkProperty.h"
#include "vtkCamera.h"
#include "vtkLight.h"
#include "vtkOpenGLPolyDataMapper.h"
#include "vtkJPEGReader.h"
#include "vtkImageData.h"
#include <vtkPNGWriter.h>

#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkPolyDataReader.h>
#include <vtkCleanPolyData.h>
#include <vtkPolyDataNormals.h>
#include <vtkPoints.h>
#include <vtkUnsignedCharArray.h>
#include <vtkFloatArray.h>
#include <vtkDoubleArray.h>
#include <vtkCellArray.h>
#include <vtkDataSetReader.h>
#include <vtkContourFilter.h>
#include <vtkRectilinearGrid.h>
#include <vtkDataSetWriter.h>
#include <vtkRectilinearGridToTetrahedra.h>
#include <vtkUnstructuredGrid.h>

#include <vtkCamera.h>
#include <vtkDataSetMapper.h>
#include <vtkRenderer.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>


// ****************************************************************************
//  Function: GetNumberOfPoints
//
//  Arguments:
//     dims: an array of size 3 with the number of points in X, Y, and Z.
//           2D data sets would have Z=1
//
//  Returns:  the number of points in a rectilinear mesh
//
// ****************************************************************************

int GetNumberOfPoints(const int* dims)
{
    // 3D
    return dims[0] * dims[1] * dims[2];
    // 2D
    //return dims[0]*dims[1];
}

// ****************************************************************************
//  Function: GetNumberOfCells
//
//  Arguments:
//
//      dims: an array of size 3 with the number of points in X, Y, and Z.
//            2D data sets would have Z=1
//
//  Returns:  the number of cells in a rectilinear mesh
//
// ****************************************************************************

int GetNumberOfCells(const int* dims)
{
    // 3D
    return (dims[0] - 1) * (dims[1] - 1) * (dims[2] - 1);
    // 2D
    //return (dims[0]-1)*(dims[1]-1);
}


// ****************************************************************************
//  Function: GetPointIndex
//
//  Arguments:
//      idx:  the logical index of a point.
//              0 <= idx[0] < dims[0]
//              1 <= idx[1] < dims[1]
//              2 <= idx[2] < dims[2] (or always 0 if 2D)
//      dims: an array of size 3 with the number of points in X, Y, and Z.
//            2D data sets would have Z=1
//
//  Returns:  the point index
//
// ****************************************************************************

int GetPointIndex(const int* idx, const int* dims)
{
    // 3D
    return idx[2] * dims[0] * dims[1] + idx[1] * dims[0] + idx[0];
    // 2D
    //return idx[1]*dims[0]+idx[0];
}


// ****************************************************************************
//  Function: GetCellIndex
//
//  Arguments:
//      idx:  the logical index of a cell.
//              0 <= idx[0] < dims[0]-1
//              1 <= idx[1] < dims[1]-1 
//              2 <= idx[2] < dims[2]-1 (or always 0 if 2D)
//      dims: an array of size 3 with the number of points in X, Y, and Z.
//            2D data sets would have Z=1
//
//  Returns:  the cell index
//
// ****************************************************************************

int GetCellIndex(const int* idx, const int* dims)
{
    // 3D
    return idx[2] * (dims[0] - 1) * (dims[1] - 1) + idx[1] * (dims[0] - 1) + idx[0];
    // 2D
    //return idx[1]*(dims[0]-1)+idx[0];
}

// ****************************************************************************
//  Function: GetLogicalPointIndex
//
//  Arguments:
//      idx (output):  the logical index of the point.
//              0 <= idx[0] < dims[0]
//              1 <= idx[1] < dims[1] 
//              2 <= idx[2] < dims[2] (or always 0 if 2D)
//      pointId:  a number between 0 and (GetNumberOfPoints(dims)-1).
//      dims: an array of size 3 with the number of points in X, Y, and Z.
//            2D data sets would have Z=1
//
//  Returns:  None (argument idx is output)
//
// ****************************************************************************

void GetLogicalPointIndex(int* idx, int pointId, const int* dims)
{
    // 3D
    idx[0] = pointId % dims[0];
    idx[1] = (pointId / dims[0]) % dims[1];
    idx[2] = pointId / (dims[0] * dims[1]);

    // 2D
    // idx[0] = pointId%dims[0];
    // idx[1] = pointId/dims[0];
}


// ****************************************************************************
//  Function: GetLogicalCellIndex
//
//  Arguments:
//      idx (output):  the logical index of the cell index.
//              0 <= idx[0] < dims[0]-1
//              1 <= idx[1] < dims[1]-1 
//              2 <= idx[2] < dims[2]-1 (or always 0 if 2D)
//      cellId:  a number between 0 and (GetNumberOfCells(dims)-1).
//      dims: an array of size 3 with the number of points in X, Y, and Z.
//            2D data sets would have Z=1
//
//  Returns:  None (argument idx is output)
//
// ****************************************************************************

void GetLogicalCellIndex(int* idx, int cellId, const int* dims)
{
    // 3D
    idx[0] = cellId % (dims[0] - 1);
    idx[1] = (cellId / (dims[0] - 1)) % (dims[1] - 1);
    idx[2] = cellId / ((dims[0] - 1) * (dims[1] - 1));

    // 2D
    //idx[0] = cellId%(dims[0]-1);
    //idx[1] = cellId/(dims[0]-1);
}


class TriangleList
{
public:
    TriangleList() { maxTriangles = 1000000; triangleIdx = 0; pts = new float[9 * maxTriangles]; };
    virtual      ~TriangleList() { delete[] pts; };

    void          AddTriangle(float X1, float Y1, float Z1, float X2, float Y2, float Z2, float X3, float Y3, float Z3);
    vtkPolyData* MakePolyData(void);

protected:
    float* pts;
    int           maxTriangles;
    int           triangleIdx;
};

void
TriangleList::AddTriangle(float X1, float Y1, float Z1, float X2, float Y2, float Z2, float X3, float Y3, float Z3)
{
    pts[9 * triangleIdx + 0] = X1;
    pts[9 * triangleIdx + 1] = Y1;
    pts[9 * triangleIdx + 2] = Z1;
    pts[9 * triangleIdx + 3] = X2;
    pts[9 * triangleIdx + 4] = Y2;
    pts[9 * triangleIdx + 5] = Z2;
    pts[9 * triangleIdx + 6] = X3;
    pts[9 * triangleIdx + 7] = Y3;
    pts[9 * triangleIdx + 8] = Z3;
    triangleIdx++;
}

vtkPolyData*
TriangleList::MakePolyData(void)
{
    int ntriangles = triangleIdx;
    int numPoints = 3 * (ntriangles);
    vtkPoints* vtk_pts = vtkPoints::New();
    vtk_pts->SetNumberOfPoints(numPoints);
    int ptIdx = 0;
    vtkCellArray* tris = vtkCellArray::New();
    tris->EstimateSize(numPoints, 4);
    for (int i = 0; i < ntriangles; i++)
    {
        double pt[3];
        pt[0] = pts[9 * i];
        pt[1] = pts[9 * i + 1];
        pt[2] = pts[9 * i + 2];
        vtk_pts->SetPoint(ptIdx, pt);
        pt[0] = pts[9 * i + 3];
        pt[1] = pts[9 * i + 4];
        pt[2] = pts[9 * i + 5];
        vtk_pts->SetPoint(ptIdx + 1, pt);
        pt[0] = pts[9 * i + 6];
        pt[1] = pts[9 * i + 7];
        pt[2] = pts[9 * i + 8];
        vtk_pts->SetPoint(ptIdx + 2, pt);
        vtkIdType ids[3] = { ptIdx, ptIdx + 1, ptIdx + 2 };
        tris->InsertNextCell(3, ids);
        ptIdx += 3;
    }

    vtkPolyData* pd = vtkPolyData::New();
    pd->SetPoints(vtk_pts);
    pd->SetPolys(tris);
    tris->Delete();
    vtk_pts->Delete();

    return pd;
}

class Tetrahedron
{
public:
    float X[4];
    float Y[4];
    float Z[4];
    float F[4];
    void PrintSelf()
    {
        for (int i = 0; i < 4; i++)
            printf("\tV%d: (%f, %f, %f) = %f\n", i, X[i], Y[i], Z[i], F[i]);
    };
};

void IsosurfaceTet(Tetrahedron& tet, TriangleList& tl, float isoval)
{
    //Failed test case - tl.AddTriangle(0.25, 0.25, 0.25, 0.5, 0.5, 0.5, 0.75, 0.75, 0.75);

    //X-Coor for Current Tet
    float tet_V0_X = tet.X[0];
    float tet_V1_X = tet.X[1];
    float tet_V2_X = tet.X[2];
    float tet_V3_X = tet.X[3];

    // Y-Coor for Current Tet
    float tet_V0_Y = tet.Y[0];
    float tet_V1_Y = tet.Y[1];
    float tet_V2_Y = tet.Y[2];
    float tet_V3_Y = tet.Y[3];

    // Z-Coor for Current Tet
    float tet_V0_Z = tet.Z[0];
    float tet_V1_Z = tet.Z[1];
    float tet_V2_Z = tet.Z[2];
    float tet_V3_Z = tet.Z[3];

    // F-Values for Current Tets Vertices
    float tet_F_V0 = tet.F[0];
    float tet_F_V1 = tet.F[1];
    float tet_F_V2 = tet.F[2];
    float tet_F_V3 = tet.F[3];

    // 6 Different t's for 6 different edges of Current Tet
    // Initialize t's
    float edge_a_t = 0.0;
    float edge_b_t = 0.0;
    float edge_c_t = 0.0;
    float edge_d_t = 0.0;
    float edge_e_t = 0.0;
    float edge_f_t = 0.0;

    //Calculate t's in general
    edge_a_t = (isoval - tet_F_V0) / (tet_F_V1 - tet_F_V0); // Uses F values from V0 and V1
    edge_b_t = (isoval - tet_F_V0) / (tet_F_V2 - tet_F_V0); // Uses F values from V0 and V2
    edge_c_t = (isoval - tet_F_V0) / (tet_F_V3 - tet_F_V0); // Uses F values from V0 and V3
    edge_d_t = (isoval - tet_F_V1) / (tet_F_V2 - tet_F_V1); // Uses F values from V1 and V2
    edge_e_t = (isoval - tet_F_V1) / (tet_F_V3 - tet_F_V1); // Uses F values from V1 and V3
    edge_f_t = (isoval - tet_F_V2) / (tet_F_V3 - tet_F_V2); // Uses F values from V2 and V3

    // Figure out which case this tet is in, piggy-backing off of the bool system I used in project 5 but with the new convention for equality to isoval
    // Intialize bools to get bitty with - V3V2V1V0
    int vertex_0_bool = 0;
    int vertex_1_bool = 0;
    int vertex_2_bool = 0;
    int vertex_3_bool = 0;

    if (tet_F_V0 >= isoval)
    {
        vertex_0_bool = 1; // classify as 1 if the field value at vertices are greater than 12.2
    }

    if (tet_F_V1 >= isoval)
    {
        vertex_1_bool = 1;
    }

    if (tet_F_V2 >= isoval)
    {
        vertex_2_bool = 1;
    }

    if (tet_F_V3 >= isoval)
    {
        vertex_3_bool = 1;
    }

    // Get the actual case number -- this will change in each iteration
    int actual_case_number = (8 * vertex_3_bool) + (4 * vertex_2_bool) + (2 * vertex_1_bool) + (1 * vertex_0_bool);

    // Interpolate!
    // In general, F(X) = F(A) + edge_a_t * (F(B) - F(A)) = point_value_x_y_z = start_vertex + some_t * (end_vertex - start_vertex)

    // Case 0 - All vertex F values are lower, so do nothing
    // Case 15 - All vertex F values are higher, so do nothing

    // Declare points for cases to be intialized in if's
    float edge_pt1_X;
    float edge_pt1_Y;
    float edge_pt1_Z;

    float edge_pt2_X;
    float edge_pt2_Y;
    float edge_pt2_Z;

    float edge_pt3_X;
    float edge_pt3_Y;
    float edge_pt3_Z;

    float edge_pt4_X;
    float edge_pt4_Y;
    float edge_pt4_Z;

    float edge_pt5_X;
    float edge_pt5_Y;
    float edge_pt5_Z;

    float edge_pt6_X;
    float edge_pt6_Y;
    float edge_pt6_Z;

    
    // Case 1 - 0001, 14 - 1110 (V3-V1 are all lower, V0 is higher OR is V3-V1 are all higher, and V0 is lower...)
    if (actual_case_number == 1 || actual_case_number == 14)
    {
        //cout << "A_T" << edge_a_t << endl;
        // cout << "B_T" << edge_b_t << endl;
        //cout << "C_T" << edge_c_t << endl;
        //cout << "CASE 1/14 HERE" << endl;
        //Edge A Point
        edge_pt1_X = tet_V0_X + edge_a_t * (tet_V1_X - tet_V0_X);
        edge_pt1_Y = tet_V0_Y + edge_a_t * (tet_V1_Y - tet_V0_Y);
        edge_pt1_Z = tet_V0_Z + edge_a_t * (tet_V1_Z - tet_V0_Z);

        // Edge B Point
        edge_pt2_X = tet_V0_X + edge_b_t * (tet_V2_X - tet_V0_X);
        edge_pt2_Y = tet_V0_Y + edge_b_t * (tet_V2_Y - tet_V0_Y);
        edge_pt2_Z = tet_V0_Z + edge_b_t * (tet_V2_Z - tet_V0_Z);

        // Edge C Point
        edge_pt3_X = tet_V0_X + edge_c_t * (tet_V3_X - tet_V0_X);
        edge_pt3_Y = tet_V0_Y + edge_c_t * (tet_V3_Y - tet_V0_Y);
        edge_pt3_Z = tet_V0_Z + edge_c_t * (tet_V3_Z - tet_V0_Z);

        tl.AddTriangle(edge_pt3_X, edge_pt3_Y, edge_pt3_Z, edge_pt2_X, edge_pt2_Y, edge_pt2_Z, edge_pt1_X, edge_pt1_Y, edge_pt1_Z); // Added 03-02-01

    }
    
    
    
    // Case 2 - 0010, 13 - 1101 (V1 is the higher vertex, and the rest are lower OR V1 is the lower vertex and the rest are higher)
    else if (actual_case_number == 2 || actual_case_number == 13)
    {
        //cout << "A_T" << edge_a_t << endl;
        //cout << "D_T" << edge_d_t << endl;
        //cout << "E_T" << edge_e_t << endl;
        // Debugging: Still spikey, but we'll see if it smooths out with the new cases
        // Edge A Point
        //cout << "CASE 2/13 HERE" << endl;
        edge_pt1_X = tet_V0_X + edge_a_t * (tet_V1_X - tet_V0_X);
        edge_pt1_Y = tet_V0_Y + edge_a_t * (tet_V1_Y - tet_V0_Y);
        edge_pt1_Z = tet_V0_Z + edge_a_t * (tet_V1_Z - tet_V0_Z);

        // Edge D Point
        edge_pt2_X = tet_V1_X + edge_d_t * (tet_V2_X - tet_V1_X);
        edge_pt2_Y = tet_V1_Y + edge_d_t * (tet_V2_Y - tet_V1_Y);
        edge_pt2_Z = tet_V1_Z + edge_d_t * (tet_V2_Z - tet_V1_Z);

        // Edge E Point
        edge_pt3_X = tet_V1_X + edge_e_t * (tet_V3_X - tet_V1_X);
        edge_pt3_Y = tet_V1_Y + edge_e_t * (tet_V3_Y - tet_V1_Y);
        edge_pt3_Z = tet_V1_Z + edge_e_t * (tet_V3_Z - tet_V1_Z);

        tl.AddTriangle(edge_pt3_X, edge_pt3_Y, edge_pt3_Z, edge_pt2_X, edge_pt2_Y, edge_pt2_Z, edge_pt1_X, edge_pt1_Y, edge_pt1_Z); // 12-03-01
    }
    

    
    // Case 3 - 0011, 12 - 1100 (V0 and V1 are both higher, OR V0 and V1 are both lower)
    else if (actual_case_number == 3 || actual_case_number == 12)
    {
        //cout << "CASE 3/12 HERE" << endl;
        // Triangle A
        // Edge B point
        edge_pt1_X = tet_V0_X + edge_b_t * (tet_V2_X - tet_V0_X);
        edge_pt1_Y = tet_V0_Y + edge_b_t * (tet_V2_Y - tet_V0_Y);
        edge_pt1_Z = tet_V0_Z + edge_b_t * (tet_V2_Z - tet_V0_Z);

        // Edge C Point
        edge_pt2_X = tet_V0_X + edge_c_t * (tet_V3_X - tet_V0_X);
        edge_pt2_Y = tet_V0_Y + edge_c_t * (tet_V3_Y - tet_V0_Y);
        edge_pt2_Z = tet_V0_Z + edge_c_t * (tet_V3_Z - tet_V0_Z);

        //Edge D Point
        edge_pt3_X = tet_V1_X + edge_d_t * (tet_V2_X - tet_V1_X);
        edge_pt3_Y = tet_V1_Y + edge_d_t * (tet_V2_Y - tet_V1_Y);
        edge_pt3_Z = tet_V1_Z + edge_d_t * (tet_V2_Z - tet_V1_Z);

        tl.AddTriangle(edge_pt1_X, edge_pt1_Y, edge_pt1_Z, edge_pt2_X, edge_pt2_Y, edge_pt2_Z, edge_pt3_X, edge_pt3_Y, edge_pt3_Z);

        // Triangle B
        // Edge C Point
        edge_pt4_X = tet_V0_X + edge_c_t * (tet_V3_X - tet_V0_X);
        edge_pt4_Y = tet_V0_Y + edge_c_t * (tet_V3_Y - tet_V0_Y);
        edge_pt4_Z = tet_V0_Z + edge_c_t * (tet_V3_Z - tet_V0_Z);

        // Edge D Point
        edge_pt5_X = tet_V1_X + edge_d_t * (tet_V2_X - tet_V1_X);
        edge_pt5_Y = tet_V1_Y + edge_d_t * (tet_V2_Y - tet_V1_Y);
        edge_pt5_Z = tet_V1_Z + edge_d_t * (tet_V2_Z - tet_V1_Z);

        //Edge E Point
        edge_pt6_X = tet_V1_X + edge_e_t * (tet_V3_X - tet_V1_X);
        edge_pt6_Y = tet_V1_Y + edge_e_t * (tet_V3_Y - tet_V1_Y);
        edge_pt6_Z = tet_V1_Z + edge_e_t * (tet_V3_Z - tet_V1_Z);

        tl.AddTriangle(edge_pt4_X, edge_pt4_Y, edge_pt4_Z, edge_pt5_X, edge_pt5_Y, edge_pt5_Z, edge_pt6_X, edge_pt6_Y, edge_pt6_Z);
    }
    
    
    
    // Case 4 - 0100, 11 - 1011 (V2 is higher and everything else is lower OR V2 is lower and all others are higher)
    else if (actual_case_number == 4 || actual_case_number == 11)
    {
        //cout << "CASE 4/11 HERE" << endl;
        // Edge B point
        edge_pt1_X = tet_V0_X + edge_b_t * (tet_V2_X - tet_V0_X);
        edge_pt1_Y = tet_V0_Y + edge_b_t * (tet_V2_Y - tet_V0_Y);
        edge_pt1_Z = tet_V0_Z + edge_b_t * (tet_V2_Z - tet_V0_Z);

        // Edge D Point
        edge_pt2_X = tet_V1_X + edge_d_t * (tet_V2_X - tet_V1_X);
        edge_pt2_Y = tet_V1_Y + edge_d_t * (tet_V2_Y - tet_V1_Y);
        edge_pt2_Z = tet_V1_Z + edge_d_t * (tet_V2_Z - tet_V1_Z);

        //Edge F Point
        edge_pt3_X = tet_V2_X + edge_f_t * (tet_V3_X - tet_V2_X);
        edge_pt3_Y = tet_V2_Y + edge_f_t * (tet_V3_Y - tet_V2_Y);
        edge_pt3_Z = tet_V2_Z + edge_f_t * (tet_V3_Z - tet_V2_Z);

        tl.AddTriangle(edge_pt1_X, edge_pt1_Y, edge_pt1_Z, edge_pt2_X, edge_pt2_Y, edge_pt2_Z, edge_pt3_X, edge_pt3_Y, edge_pt3_Z);
    }
    

    
    // Case 5 - 0101, 10 - 1010 (V0 and V2 are either both higher OR V0 and V2 are both lower)
    else if (actual_case_number == 5 || actual_case_number == 10)
    {
        //cout << "CASE 5/10 HERE" << endl;
        // Triangle A
        // Edge A point
        edge_pt1_X = tet_V0_X + edge_c_t * (tet_V3_X - tet_V0_X);
        edge_pt1_Y = tet_V0_Y + edge_c_t * (tet_V3_Y - tet_V0_Y);
        edge_pt1_Z = tet_V0_Z + edge_c_t * (tet_V3_Z - tet_V0_Z);

        // Edge C Point
        edge_pt2_X = tet_V1_X + edge_d_t * (tet_V2_X - tet_V1_X);
        edge_pt2_Y = tet_V1_Y + edge_d_t * (tet_V2_Y - tet_V1_Y);
        edge_pt2_Z = tet_V1_Z + edge_d_t * (tet_V2_Z - tet_V1_Z);

        //Edge F Point
        edge_pt3_X = tet_V2_X + edge_f_t * (tet_V3_X - tet_V2_X);
        edge_pt3_Y = tet_V2_Y + edge_f_t * (tet_V3_Y - tet_V2_Y);
        edge_pt3_Z = tet_V2_Z + edge_f_t * (tet_V3_Z - tet_V2_Z);

        tl.AddTriangle(edge_pt1_X, edge_pt1_Y, edge_pt1_Z, edge_pt2_X, edge_pt2_Y, edge_pt2_Z, edge_pt3_X, edge_pt3_Y, edge_pt3_Z);

        // Triangle B
        // Edge C point
        edge_pt4_X = tet_V0_X + edge_a_t * (tet_V1_X - tet_V0_X);
        edge_pt4_Y = tet_V0_Y + edge_a_t * (tet_V1_Y - tet_V0_Y);
        edge_pt4_Z = tet_V0_Z + edge_a_t * (tet_V1_Z - tet_V0_Z);

        // Edge D Point
        edge_pt5_X = tet_V0_X + edge_c_t * (tet_V3_X - tet_V0_X);
        edge_pt5_Y = tet_V0_Y + edge_c_t * (tet_V3_Y - tet_V0_Y);
        edge_pt5_Z = tet_V0_Z + edge_c_t * (tet_V3_Z - tet_V0_Z);

        //Edge F Point
        edge_pt6_X = tet_V1_X + edge_d_t * (tet_V2_X - tet_V1_X);
        edge_pt6_Y = tet_V1_Y + edge_d_t * (tet_V2_Y - tet_V1_Y);
        edge_pt6_Z = tet_V1_Z + edge_d_t * (tet_V2_Z - tet_V1_Z);

        tl.AddTriangle(edge_pt4_X, edge_pt4_Y, edge_pt4_Z, edge_pt5_X, edge_pt5_Y, edge_pt5_Z, edge_pt6_X, edge_pt6_Y, edge_pt6_Z);

    }
    

    
    // Case 6 - 0110, 9 - 1001 (V1, V2 are lower and V0, V3 are higher OR V0, V3 are higher, V1, V2 are lower)
    // THESE CASES SHOULD Have Two ADDTRIANGLE CALLS
    else if (actual_case_number == 6  || actual_case_number == 9)
    {
        // cout << "Case 6/9 HERE" << endl;
        
        // Triangle A
        // Edge B Point
        edge_pt1_X = tet_V0_X + edge_a_t * (tet_V1_X - tet_V0_X);
        edge_pt1_Y = tet_V0_Y + edge_a_t * (tet_V1_Y - tet_V0_Y);
        edge_pt1_Z = tet_V0_Z + edge_a_t * (tet_V1_Z - tet_V0_Z);

        // Edge E Point
        edge_pt2_X = tet_V0_X + edge_b_t * (tet_V2_X - tet_V0_X);
        edge_pt2_Y = tet_V0_Y + edge_b_t * (tet_V2_Y - tet_V0_Y);
        edge_pt2_Z = tet_V0_Z + edge_b_t * (tet_V2_Z - tet_V0_Z);

        // Edge F Point
        edge_pt3_X = tet_V2_X + edge_f_t * (tet_V3_X - tet_V2_X);
        edge_pt3_Y = tet_V2_Y + edge_f_t * (tet_V3_Y - tet_V2_Y);
        edge_pt3_Z = tet_V2_Z + edge_f_t * (tet_V3_Z - tet_V2_Z);

        tl.AddTriangle(edge_pt1_X, edge_pt1_Y, edge_pt1_Z, edge_pt2_X, edge_pt2_Y, edge_pt2_Z, edge_pt3_X, edge_pt3_Y, edge_pt3_Z);


        // Triangle B
        // Edge A Point
        edge_pt4_X = tet_V0_X + edge_a_t * (tet_V1_X - tet_V0_X);
        edge_pt4_Y = tet_V0_Y + edge_a_t * (tet_V1_Y - tet_V0_Y);
        edge_pt4_Z = tet_V0_Z + edge_a_t * (tet_V1_Z - tet_V0_Z);
        
        // Edge E Point
        edge_pt5_X = tet_V1_X + edge_e_t * (tet_V3_X - tet_V1_X);
        edge_pt5_Y = tet_V1_Y + edge_e_t * (tet_V3_Y - tet_V1_Y);
        edge_pt5_Z = tet_V1_Z + edge_e_t * (tet_V3_Z - tet_V1_Z);

        // Edge F Point
        edge_pt6_X = tet_V2_X + edge_f_t * (tet_V3_X - tet_V2_X);
        edge_pt6_Y = tet_V2_Y + edge_f_t * (tet_V3_Y - tet_V2_Y);
        edge_pt6_Z = tet_V2_Z + edge_f_t * (tet_V3_Z - tet_V2_Z);

        tl.AddTriangle(edge_pt4_X, edge_pt4_Y, edge_pt4_Z, edge_pt5_X, edge_pt5_Y, edge_pt5_Z, edge_pt6_X, edge_pt6_Y, edge_pt6_Z);
    }
    
    
    
    // Case 7 - 0111, 8 - 1000 (V3 is the higher vertex and all others are lower OR V3 is lower and all rest are higher)
    else if (actual_case_number == 7 || actual_case_number == 8)
    {
    //cout << "CASE 7/8 HERE" << endl;
    // Edge C point
    edge_pt1_X = tet_V0_X + edge_c_t * (tet_V3_X - tet_V0_X);
    edge_pt1_Y = tet_V0_Y + edge_c_t * (tet_V3_Y - tet_V0_Y);
    edge_pt1_Z = tet_V0_Z + edge_c_t * (tet_V3_Z - tet_V0_Z);

    // Edge E Point
    edge_pt2_X = tet_V1_X + edge_e_t * (tet_V3_X - tet_V1_X);
    edge_pt2_Y = tet_V1_Y + edge_e_t * (tet_V3_Y - tet_V1_Y);
    edge_pt2_Z = tet_V1_Z + edge_e_t * (tet_V3_Z - tet_V1_Z);

    //Edge F Point
    edge_pt3_X = tet_V2_X + edge_f_t * (tet_V3_X - tet_V2_X);
    edge_pt3_Y = tet_V2_Y + edge_f_t * (tet_V3_Y - tet_V2_Y);
    edge_pt3_Z = tet_V2_Z + edge_f_t * (tet_V3_Z - tet_V2_Z);

    tl.AddTriangle(edge_pt1_X, edge_pt1_Y, edge_pt1_Z, edge_pt2_X, edge_pt2_Y, edge_pt2_Z, edge_pt3_X, edge_pt3_Y, edge_pt3_Z);
        
    }
    
    
}

int main()
{
    int  i, j;

    //If you want to try on one tetrahedron.
        /*
        Tetrahedron t;
        t.X[0] = 0;
        t.Y[0] = 0;
        t.Z[0] = 0;
        t.F[0] = 0.6;
        t.X[1] = 1;
        t.Y[1] = 0;
        t.Z[1] = 0;
        t.F[1] = 0;
        t.X[2] = 0;
        t.Y[2] = 1;
        t.Z[2] = 0;
        t.F[2] = 0;
        t.X[3] = 0.5;
        t.Y[3] = 0.5;
        t.Z[3] = 1.0;
        t.F[3] = 1;
        TriangleList tl;
        IsosurfaceTet(t, tl, 0.5);
        */

    vtkDataSetReader* rdr = vtkDataSetReader::New();
    rdr->SetFileName("proj6.vtk");
    rdr->Update();
    if (rdr->GetOutput() == NULL || rdr->GetOutput()->GetNumberOfCells() == 0)
    {
        cerr << "Could not find input file." << endl;
        exit(EXIT_FAILURE);
    }

    vtkUnstructuredGrid* ugrid = (vtkUnstructuredGrid*)rdr->GetOutput();
    float* pts = (float*)ugrid->GetPoints()->GetVoidPointer(0);
    float* F = (float*)ugrid->GetPointData()->GetScalars()->GetVoidPointer(0);
    vtkCellArray* cell_array = ugrid->GetCells();

    TriangleList tl;
    cell_array->InitTraversal();
    int ncells = cell_array->GetNumberOfCells();
    cerr << "Number of cells to tetrahedralize is " << ncells << endl;
    int cnt = 0;
    float isoval = 12.2;
    for (int i = 0; i < ncells; i++)
    {
        vtkIdType npts;
        vtkIdType* ids;
        cell_array->GetNextCell(npts, ids);
        if (npts == 4)
        {
            Tetrahedron tet;
            for (int j = 0; j < 4; j++)
            {
                // This data set is in a huge bounding box.  Normalize as we go.
                tet.X[j] = (pts[3 * ids[j]] + 3e+7) / 6e+7;
                tet.Y[j] = (pts[3 * ids[j] + 1] + 3e+7) / 6e+7;
                tet.Z[j] = (pts[3 * ids[j] + 2] + 3e+7) / 6e+7;
                tet.F[j] = F[ids[j]];
            }
            IsosurfaceTet(tet, tl, isoval);
        }
        else
        {
            cerr << "Input was non-tetrahedron!!  Ignoring..." << endl;
            cerr << "Type is " << npts << endl;
            cerr << "Cell is " << i << endl;
            cnt++;
            continue;
        }
    }

    vtkPolyData* pd = tl.MakePolyData();

    /*
        //This can be useful for debugging
        vtkDataSetWriter *writer = vtkDataSetWriter::New();
        writer->SetFileName("proj6_out.vtk");
        writer->SetInputData(pd);
        writer->Write();
     */

    vtkCleanPolyData* cpd = vtkCleanPolyData::New();
    cpd->SetInputData(pd);
    cpd->SetAbsoluteTolerance(0);
    cpd->PointMergingOn();
    cpd->Update();
    vtkPolyDataNormals* pdn = vtkPolyDataNormals::New();
    pdn->SetInputData(cpd->GetOutput());
    //pdn->SetInputData(pd);
    pdn->Update();

    vtkSmartPointer<vtkDataSetMapper> win1Mapper =
        vtkSmartPointer<vtkDataSetMapper>::New();
    win1Mapper->SetInputData(pdn->GetOutput());
    win1Mapper->SetScalarRange(0, 0.15);

    vtkSmartPointer<vtkActor> win1Actor =
        vtkSmartPointer<vtkActor>::New();
    win1Actor->SetMapper(win1Mapper);

    vtkSmartPointer<vtkRenderer> ren1 =
        vtkSmartPointer<vtkRenderer>::New();

    vtkSmartPointer<vtkRenderWindow> renWin =
        vtkSmartPointer<vtkRenderWindow>::New();
    renWin->AddRenderer(ren1);

    vtkSmartPointer<vtkRenderWindowInteractor> iren =
        vtkSmartPointer<vtkRenderWindowInteractor>::New();
    iren->SetRenderWindow(renWin);
    ren1->AddActor(win1Actor);
    ren1->SetBackground(0.0, 0.0, 0.0);
    renWin->SetSize(800, 800);

    ren1->GetActiveCamera()->SetFocalPoint(0.5, 0.5, 0.5);
    ren1->GetActiveCamera()->SetPosition(0, 0, -2);
    ren1->GetActiveCamera()->SetViewUp(0, 1, 0);
    ren1->GetActiveCamera()->SetClippingRange(0.01, 4);
    //ren1->GetActiveCamera()->SetDistance(30);

    // This starts the event loop and invokes an initial render.
    //
    iren->Initialize();
    iren->Start();

    pd->Delete();
}
