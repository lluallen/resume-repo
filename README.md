# README #

This documents how to run a scientific visualization of a supernova using CMake with CMakeLists.txt, and VTK with the source code supernova.cxx.
This visualization techinique uses isosurfacing with vector interpolation along tetrahedrons. 

## POTENTIAL EMPLOYERS ##
* How do I run it?
	On a Windows machine, navigate to the Debug build folder within this repo, and double-click on the supernova executable. You could also use the windows Command Prompt to launch the executable by simply navigating to the build folder and typing "supernova.exe" and hitting Enter. 
	Try clicking around on the window that pops up with the supernova visualization. I encourage you to play around with viewing the star by pressing "w" and "s" and interacting with the pop-up window. 
## DEVELOPERS ##
* Dependencies:
	* CMake - A cross-platform software packaging tool (https://cmake.org/)
	* VTK - Visualization ToolKit (https://vtk.org/)
	* Visual Studio 2019 - I use Visual Studio to run VTK / CMake programs. If you figure out a better way that you wish to share, please send feedback to lluallen@uoregon.edu (https://visualstudio.microsoft.com/)
* How do I run it?
	If you want to just simply produce the picture and not have control over the output as a developer, then follow the above instructions for "Potential Employers". Otherwise...
	* 1) Download the above dependencies (VTK, CMake, and Visual Studio 2019)
	* 2) Clone this repository - The folder structure / naming scheme is set up so that CMake and VTK can talk to each other, so don't change any names unless you know how they will affect CMake. 
	* 3) CMake is installed, and there should be a build folder in this repository. Within this build folder is the VTK data file supernova.vtk. This data file needs to go into the "Debug" folder (and eventually the folder that the executable will be invoked from) after you build your first solution with Viual Studio. Open the CMakeLists.txt file and edit the path to your VTK installation. This will be the first error you run into in trying to use CMake and VTK if you don't edit this file before clicking "Configure" in CMake. Open CMake, and set the Source path to be this entire supernova repo. Then set the Build path to be the build folder in the supernova repo. Click Configure and it will prompt you to choose the generator. Choose Visual Studio 2019. Click Configure again, then hit Generate. Then, when you Open the solution it will open in Visual Studio 2019. 
	* 4) You should now see your solution in Visual Studio. Right click on the ALL_BUILD folder to the right of the screen, and then click "Build". Now hit F5, or press "Local Windows Debugger" at the top.
		- If you did not start Visual Studio as an administrator (not the default) an error message will occur within Visual Studio that says "Accessed denied". This is fine. Just navigate through the now very full Debug folder within the build folder, and double click the supernova executable. Make sure supernova.vtk is in the same drirectory as where you're trying to run the executable. 
	* 5) Please refer to the supernova screenshot in this repo for a reference of what the output should look like. The program will take about 10 seconds or less to load in the data file and generate the supernova in an OpenGL pop-up window. If you want to click the pop-up window and play around with the picture, you can do that. 
		- You'll also get some pretty interesting results if you press "w" with the pop-up window open. You can press "s" to undo this action. 


### Who do I talk to? ###

* Lindsay Luallen
* lluallen@uoregon.edu